<?php
/**
 * @file
 * root_cafe_feature.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function root_cafe_feature_taxonomy_default_vocabularies() {
  return array(
    'event_types' => array(
      'name' => 'Event Types',
      'machine_name' => 'event_types',
      'description' => 'Choose the type of event that this event represents',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'menu_section' => array(
      'name' => 'Menu Section',
      'machine_name' => 'menu_section',
      'description' => 'The menu section that this item belongs to',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'menu_type' => array(
      'name' => 'Menu Type',
      'machine_name' => 'menu_type',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
