<?php

$plugin = array(
  'title' => t('Render the Root Cafe Menu'),
  'single' => TRUE,
  'category' => t('Root Cafe'),
  // Despite having no "settings" we need this function to pass back a form, or we'll loose the context and title settings.
  //'edit form' => 'module_content_type_edit_form',
  'render callback' => 'cafe_menu_render',
);

function cafe_menu_render($subtype, $conf, $args, $context = NULL) {
  $block = new stdClass;
  $content = "<div id='menusContainer'></div>
<script type='text/javascript' src='http://menus.singleplatform.co/businesses/storefront/?apiKey=ke09z8icq4xu8uiiccighy1bw'>
</script>
<script>
    var options = {};
    options['PrimaryBackgroundColor'] = '#ffffff';
    options['MenuDescBackgroundColor'] = '#ffffff';
    options['SectionTitleBackgroundColor'] = '#c2cf65';
    options['SectionDescBackgroundColor'] = '#c2cf65';
    options['ItemBackgroundColor'] = '#ffffff';
    options['PrimaryFontFamily'] = 'Droid Sans';
    options['BaseFontSize'] = '16px';
    options['FontCasing'] = 'Lowercase';
    options['PrimaryFontColor'] = '#000000';
    options['MenuDescFontColor'] = '#000000';
    options['SectionTitleFontColor'] = '#2a4523';
    options['SectionDescFontColor'] = '#2a4523';
    options['ItemTitleFontColor'] = '#2a4523';
    options['FeedbackFontColor'] = '#2a4523';
    options['ItemDescFontColor'] = '#2a4523';
    options['ItemPriceFontColor'] = '#2a4523';
    options['HideDisplayOptionPhotos'] = 'true';
    options['HideDisplayOptionDisclaimer'] = 'true';
    options['MenuTemplate'] = '2';
    //options['MenuDropDownBackgroundColor'] = '#acf489';
    new BusinessView('the-root-cafe', 'menusContainer', options);
</script>
<style>
#sp_main {
  max-width:100%;
}
</style>";
  $block->content = $content;

  return $block;
}
