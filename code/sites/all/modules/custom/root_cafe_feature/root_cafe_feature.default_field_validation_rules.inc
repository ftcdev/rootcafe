<?php
/**
 * @file
 * root_cafe_feature.default_field_validation_rules.inc
 */

/**
 * Implements hook_default_field_validation_rule().
 */
function root_cafe_feature_default_field_validation_rule() {
  $export = array();

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'At least 48 hours notice';
  $rule->name = 'at_least_48_hours_notice';
  $rule->field_name = 'field_bakery_datetime';
  $rule->col = 'value';
  $rule->entity_type = 'entityform';
  $rule->bundle = 'bakery_form';
  $rule->validator = 'field_validation_date_range2_validator';
  $rule->settings = array(
    'cycle' => 'global',
    'min' => '+53 hours',
    'max' => '',
    'reverse' => 0,
    'bypass' => 0,
    'roles' => array(
      2 => 0,
      3 => 0,
      4 => 0,
    ),
    'errors' => 1,
  );
  $rule->error_message = 'The order must be placed 48 hours in advance, sorry!';
  $export['at_least_48_hours_notice'] = $rule;

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Email is Valid';
  $rule->name = 'email_is_valid';
  $rule->field_name = 'field_form_email';
  $rule->col = 'value';
  $rule->entity_type = 'entityform';
  $rule->bundle = 'bakery_form';
  $rule->validator = 'field_validation_email_validator';
  $rule->settings = array(
    'bypass' => 0,
    'roles' => array(
      2 => 0,
      3 => 0,
      4 => 0,
    ),
    'errors' => 1,
  );
  $rule->error_message = 'Ensure you are providing a valid email address';
  $export['email_is_valid'] = $rule;

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Phone Validation';
  $rule->name = 'phone_validation';
  $rule->field_name = 'field_form_phone';
  $rule->col = 'value';
  $rule->entity_type = 'entityform';
  $rule->bundle = 'bakery_form';
  $rule->validator = 'field_validation_phone_validator';
  $rule->settings = array(
    'country' => 'ca',
    'bypass' => 0,
    'roles' => array(
      2 => 0,
      3 => 0,
      4 => 0,
    ),
    'errors' => 1,
  );
  $rule->error_message = 'Please enter a valid phone number';
  $export['phone_validation'] = $rule;

  return $export;
}
