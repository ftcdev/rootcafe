<?php

/**
 * @file template.php
 */


function rootcafe_panels_ajax_tab_tabs($vars) {
  foreach ($vars['tabs'] as $tab) {
    $url_enabled = (isset($tab['url_enabled']) ? $tab['url_enabled'] : 1);

    if (strpos($_SERVER['REQUEST_URI'], '?') !== FALSE) {
      $sourcestring = preg_replace('/^[^\?]+(\?.*)/','$1',$_SERVER['REQUEST_URI']);
      $sourcestring = preg_replace('/^(.*)&panels_ajax_tab_tab=.*/','$1',$sourcestring);
      $tabs[] = '<a href="' . $tab['href'] . $sourcestring . '" class="panels-ajax-tab-tab" data-panel-name="' . $tab['mini_panel']->name . '" data-target-id="' . $vars['tab_container_id'] . '" data-entity-context="' . $vars['context_string'] . '" data-url-enabled="' . $url_enabled . '">' . $tab['title'] . '</a>';
    } else {
      $tabs[] = '<a href="' . $tab['href'] . '" class="panels-ajax-tab-tab" data-panel-name="' . $tab['mini_panel']->name . '" data-target-id="' . $vars['tab_container_id'] . '" data-entity-context="' . $vars['context_string'] . '" data-url-enabled="' . $url_enabled . '">' . $tab['title'] . '</a>';
    }
  }
  return theme('item_list', array('items' => $tabs, 'attributes' => array('class' => array('tabs', 'inline', 'panels-ajax-tab', 'nav', 'nav-stacked', 'nav-pills'))));
}

/**
 * Implements hook_preprocess_table().
 */
function rootcafe_preprocess_table(&$variables)
{
  if (isset($variables['attributes']['class']) && is_string($variables['attributes']['class'])) {
    // Convert classes to an array.
    $variables['attributes']['class'] = explode(' ', $variables['attributes']['class']);
  }
  $variables['attributes']['class'][] = 'table';
  $variables['attributes']['class'][] = 'table-striped';
  $variables['attributes']['class'][] = 'table-bordered';
}


/**
 * Implements hook_preprocess_views_view_table().
 */
function rootcafe_preprocess_views_view_table(&$vars)
{
  $vars['classes_array'][] = 'table';
  $vars['classes_array'][] = 'table-striped';
  $vars['classes_array'][] = 'table-bordered';
}
/**
 * Implements hook_preprocess_views_view_table().
 */
function rootcafe_preprocess_views_secondary_row_view_table(&$vars)
{
  $vars['classes_array'][] = 'table';
  $vars['classes_array'][] = 'table-striped';
  $vars['classes_array'][] = 'table-bordered';
}

/**
 * Template for oEmbed theme.
 */
function rootcafe_preprocess_oembed_alter(&$vars, $hook) {
  $embed = $vars['embed'];
  if ($embed) {

    $vars['original_url'] = $embed->original_url;


    $vars['classes_array'][] = 'oembed-'. $embed->type;
    $vars['title_attributes_array']['class'] = 'oembed-title';
    $vars['content_attributes_array']['class'] = 'oembed-content';

    // oEmbed links render using core link theme unless other preprocess
    // functions suggest other hooks.
    if ($embed->type == 'link') {
      $vars['title_attributes_array']['class'] .= ' oembed-link';

      $vars['theme_hook_suggestions'][] = 'link';
      $vars['path'] = $embed->original_url;
      $vars['text'] = $embed->title;
      $vars['options'] = array(
        'absolute' => TRUE,
        'attributes' => $vars['title_attributes_array'],
        'html' => TRUE,
      );
    }
  }
}


function rootcafe_preprocess_page(&$vars){
      $vars['feature_display'] = views_embed_view('fronpage_feature', 'main');
}
function rootcafe_menu_tree__menu_block($vars) {
    return '<ul class="nav nav-pills nav-stacked">' . $vars['tree'] . '</ul>';
}
