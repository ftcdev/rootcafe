module.exports = function(grunt) {

  grunt.initConfig({

less: {
  development: {
    options: {
      paths: ["less"]
    },
    files: {
      "css/style.css": "less/style.less"
    }
  },
  production: {
    options: {
      paths: ["assets/css"],
      cleancss: true
    },
    files: {
      "css/build.css": "less/style.less"
    }
  }
},

cssmin: {
  production: {
    files: {
      'css/style.css': ['css/build.css']
    }
  }
}

});

grunt.loadNpmTasks('grunt-contrib-less');
grunt.loadNpmTasks('grunt-contrib-cssmin');

grunt.registerTask('default', ['less', 'cssmin']);
};
